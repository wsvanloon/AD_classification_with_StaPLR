# Code repository accompanying "Analyzing hierarchical multi-view MRI data with StaPLR: An application to Alzheimer's disease classification"


## Overview of included files

The following files are included in this repository:

* MVS_on_SLURM.R - A file used for running StaPLR with 3 levels on a SLURM HPC cluster
* MVS_measures_on_SLURM.R - A file used for running StaPLR with 2 levels using MRI measures as views on a SLURM HPC cluster
* MVS_scantypes_on_SLURM.R - A file used for running StaPLR with 2 levels using scan types as views on a SLURM HPC cluster
* elastic_on_slurm.R - A file used for running the elastic net on a SLURM HPC cluster
* predict_from_MVS.R - A file used for making out-of-fold predictions and saving model coefficients for StaPLR with 3 levels
* predict_from_MVS_measures.R - A file used for making out-of-fold predictions and saving model coefficients for StaPLR with 2 levels (MRI measures only)
* predict_from_MVS_scantypes.R - A file used for making out-of-fold predictions and saving model coefficients for StaPLR with 2 levels (scan types only)
* predict_from_elastic.R - A file used for making out-of-fold predictions and saving model coefficients for the elastic net
* elastic.R - A file containg the fitting function used for the elastic net
* kfolds.R - A file used for partitioning the data into k folds
* This README

The following files are NOT included in this repository:

* Shell scripts used for repeatedly calling MVS_on_SLURM.R and elastic_on_slurm.R
* The R package 'multiview'. This package can be found at https://gitlab.com/wsvanloon/multiview/-/tree/v0.3.1 or https://doi.org/10.5281/zenodo.4630669 
* The multimodal MRI data set

## Additional information

For those unfamiliar with the multiview package the file MVS_on_SLURM.R may not be easy to understand, especially since the MRI data is not included. We therefore provide a brief explanation here. It is based around the following call to fit the model:

```R
fit <- MVS(x = predictors[fold_id != rep_folds, ], y = Alzheimer[fold_id != rep_folds],
           views = viewIndexMatrix, type="StaPLR", levels=3, alphas=c(0,1,1), nnc=c(0,1,1))
```

Here `predictors` is the complete standardized feature matrix, where the number of rows is equal to the number of people (249), and the number of columns is equal to the total number of features (2,876,515). The vector `Alzheimer` is a binary vector denoting whether a person was diagnosed with probable AD (1) or is a control (0). From both of these objects only the observations NOT used for testing the model are used in fitting the model, as evidenced by `fold_id != rep_folds`. The matrix `viewIndexMatrix` is a matrix with 2 columns and 2,876,515 rows. The first column specifies an integer from 1 to 3 denoting the scan type from which each feature was derived. The second column specifies an integer from 1 to 40 denoting the MRI measure the feature belongs to. The vector `alphas` is used to specify that an L2 penalty should be used at the base level, and an L1 penalty at the higher levels. The vector `nnc` is used to specify that the L1 penalized models should contain nonnegativity constraints. The model evaluation is then performed in predict_from_MVS.R using a `predict()` method for the S3 class MVS.


